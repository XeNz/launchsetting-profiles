using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;

namespace launchSettings
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.ConfigureAppConfiguration(((context, builder) =>
                    {
                        builder.Sources.Clear();
                        var env = context.HostingEnvironment;
                        builder.SetBasePath(Path.Combine(env.ContentRootPath, "_config"));
                        builder.AddJsonFile($"dataaccess.{env.EnvironmentName.ToLowerInvariant()}.json", true);
                        builder.AddEnvironmentVariables();
                    }));
                    webBuilder.UseStartup<Startup>();
                });
    }
}