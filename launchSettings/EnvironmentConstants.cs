using System.Collections.Generic;

namespace launchSettings
{
    public class EnvironmentConstants
    {
        public static string Development = "Development";
        public static string Local = "Local";
        public static string Acceptance = "Acceptance";

        public static readonly string[] AllEnvironments = {Acceptance, Development, Local};
    }
}